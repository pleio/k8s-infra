{{- define "virusscanner.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "virusscanner.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "virusscanner.labels" -}}
helm.sh/chart: {{ include "virusscanner.chart" . }}
{{ include "virusscanner.selectorLabels" . }}
{{- end -}}

{{- define "virusscanner.scannerPort" -}}
{{- "scannerport" -}}
{{- end -}}

{{- define "virusscanner.selectorLabels" -}}
app.kubernetes.io/name: {{ include "virusscanner.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Component }}
app.kubernetes.io/component: {{ .Component }}
{{- end }}
{{- end -}}
