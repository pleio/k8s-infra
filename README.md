# Kubernetes Infrastructure

This repo contains a collection of tools that we've installed on the kubernetes clusters. This is an effort to more easily reproduce the infrastructure of a cluster, setup test environments more quickly and make this work more traceable.

## Helm 101

Helm charts help you install and configure multiple kubernetes resources with a single command. Helm keeps track of installed instances using the helm release name. You can check the instances on your kubernetes cluster with:
```bash
helm list -A
```
Installing is done with
```bash
helm install -n <namespace> <release-name> <path-to-chart>
```
This will install the chart, configuring any variables with the default values, defined in the chart's `values.yaml` file. Providing overriding values can be done by providing a values file through the `-f` flag. `install` is safe to use as it will not override existing instancse with the same name. For that, use the `upgrade` command.

Finally uninstall a chart and all kubernetes resources it created with
```bash
helm uninstall -n <namespace> <release-name>
```

## Ingress-Nginx

Ingress-Nginx acts as a load balancer for our system. Installing the chart requires an installation of the cert-manager chart as it also includes a ClusterIssuer. There is currently no usecase where we do not want the possibility of Letsencrypt certificates for tls, so we require a ClusterIssuer alongside the deployment of ingress-nginx.

On ODC-Noord, the cluster cannot provide a LoadBalancer service out of the box. Hence we require NodePort services. Additional steps have been taken to setup a LoadBalancer for the OpenStack environment. These have been derived from the [OpenStack Load balancer cook book](https://docs.openstack.org/octavia/latest/user/guides/basic-cookbook.html)
1. Create a LoadBalancer
1. Create listeners for HTTP and HTTPS and assign them to the loadbalancer
1. Create a Pool for each listener
1. Add each worker node as a Member to the Pool with the NodePort for HTTP and HTTPS
1. Assign a FLoating IP to the load LoadBalancer

## ClamAV

ClamAV is a virusscanner that works over a TCP socket. Using [TCP commands](https://docs.clamav.net/manual/Usage/Scanning.html) you can perform a scan on a file. [Clamd](https://pypi.org/project/clamd/) is likely a Python library that can be used in conjunction with it, or at least provides some inspiration on its usage.

## EFS-provisioner

A tool that mounts efs volumes whenever a pvc is made with the appriopriate storage class. Editing the existing deployment can be done as follows:
```
helm repo add aws-efs-csi-driver https://kubernetes-sigs.github.io/aws-efs-csi-driver/
helm repo update
helm upgrade -n kube-system aws-efs-csi-driver aws-efs-csi-driver/aws-efs-csi-driver -f values.efs.aws.yml
```

## Cert-Manager

A service that obtains TLS certificates for the ingresses in the cluster. Note that the ClusterIssuer that is required for this is managed through the ingress-nginx helm chart.
```
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm upgrade -n cert-manager cert-manager jetstack/cert-manager -f values.cm.aws.yaml
```
